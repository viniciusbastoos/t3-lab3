package dawson;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
  
    @Test
    public void testMethod(){
        App app = new App();

        int result = app.echo(5);

        int expected = 5;

        String message = "Testing the echo method with input 5";

        assertEquals(message, expected, result);
    } 

    @Test
    public void testMethod2(){
        App app = new App();

        int oneMore = app.oneMore(6);

        int expected = 7;

        String message = "Testing the echo method with input 6 to receive one more (maybe not)";
        assertEquals(message, expected, oneMore);
    }

}
